// THE RULES:

// Bet $1: Center Lane - PUSH ONE OF MOST KEYS ON THE KEYBOARD
// Bet $2: Upper Lane - PUSH 2
// Bet $3: Bottom Lane - PUSH 3
// Bet $4: Top Cross Lane - PUSH 4
// Bet $5+: Bottom Cross Lane - PUSH 5

// Each 2X$ in lane included in bet multiplies winnings by 2

// Any three Basic Symbols pay 2x
// Any three Brand Logos pay 5x
// Any three Jackpots pay 20x

// Basic Symbol match pays 10x
// Brand Logo match pays 20x
// Three Jackpot match pays 40x
// Double Three Jackpot match pays 100x
// Triple Three Jackpot match pays 200x
// 2X$ match pays 1000x

import create from "zustand";

const [, API] = create(() => ({
  player: {
    purse: 10,
    pot: 0,
    purseReducer: function (param) {
      this.purse += param;
      console.log("Reducer: Purse - Called with param " + param + ".");
      return null;
    },
    potReducer: function (param) {
      this.pot += param;
      console.log("Reducer: Pot - Called with param " + param + ".");
      return null;
    },
    potEmptyReducer: function () {
      this.pot = 0;
      console.log("Reducer: PotEmpty - Initialized.");
      return null;
    },
  },
  round: {
    count: 0,
    roundReducer: function () {
      this.count += 1;
      console.log("Reducer: Round - Increased the Round by 1.");
      return null;
    }
  },
  reels: {
    reel0: [],
    reel1: [],
    reel2: [],
    reel0Reducer: function (param) {
      this.reel0 = param;
      console.log("Reducer: Reel0 - Called with param " + param + ".");
      return null;
    },
    reel1Reducer: function (param) {
      this.reel1 = param;
      console.log("Reducer: Reel1 - Called with param " + param + ".");
      return null;
    },
    reel2Reducer: function (param) {
      this.reel2 = param;
      console.log("Reducer: Reel2 - Called with param " + param + ".");
      return null;
    },
    reelsReducer: function (param) {
      this.reel0 = param[0];
      this.reel1 = param[1];
      this.reel2 = param[2];
      console.log("Reducer: Reels - Called with param " + param + ".");
      return null;
    },
    reelVisibleReducer: function () {
      console.log("Reducer: ReelVisible - Initialized.");
      return [this.reel0[0], this.reel0[1], this.reel0[2], this.reel1[0], this.reel1[1], this.reel1[2], this.reel2[0], this.reel2[1], this.reel2[2],];
    },
  }
}));
export default API;
