import API from "./API";
import Reels from "../components/Reels";

const PlaceBet = (param) => {
  let player = API.getState().player;
  if (param > player.purse) {
    console.log("Action: PlaceBet - You don't have enough money for that bet!");
  } else {
    console.log("Action: PlaceBet - Moved $" + param + " from the Player's Purse to the Pot.");
    player.purseReducer(-1 * param);
    player.potReducer(param);
  }
};

const InitCabinet = () => {
  let reels = API.getState().reels;
  console.log("Action: InitCabinet - Intialized.");
  reels.reelsReducer(Reels());
};

const SpinReels = (rand0, rand1, rand2) => {
  let reels = API.getState().reels;
  let round = API.getState().round;
  round.roundReducer();
  let reel0 = reels.reel0;
  let reel1 = reels.reel1;
  let reel2 = reels.reel2;
  let i0, i1, i2, i3, i4, i5, cache0, cache1, cache2 = 0;
  //Takes rand0 as the number of slots to spin on the first reel
  for (i0 = 0; i0 < rand0; i0++) {
    //Caches the first slot value on the reel
    cache0 = reel0[0];
    //Sets every slot value (but the very last) to the one above it on the reel
    for (i1 = 0; i1 < reel0.length; i1++) {
      reel0[i1] = reel0[i1 + 1];
    }
    //Sets the last slot on the reel to the cached first value
    reel0[reel0.length - 1] = cache0;
  }
  //Takes rand1 as the number of slots to spin on the second reel
  for (i2 = 0; i2 < rand1; i2++) {
    //Caches the first slot value on the reel
    cache1 = reel1[0];
    //Sets every slot value (but the very last) to the one above it on the reel
    for (i3 = 0; i3 < reel1.length; i3++) {
      reel1[i3] = reel1[i3 + 1];
    }
    //Sets the last slot on the reel to the cached first value
    reel1[reel1.length - 1] = cache1;
  }
  //Takes rand2 as the number of slots to spin on the third reel
  for (i4 = 0; i4 < rand2; i4++) {
    //Caches the first slot value on the reel
    cache2 = reel2[0];
    //Sets every slot value (but the very last) to the one above it on the reel
    for (i5 = 0; i5 < reel2.length; i5++) {
      reel2[i5] = reel2[i5 + 1];
    }
    //Sets the last slot on the reel to the cached first value
    reel2[reel2.length - 1] = cache2;
  }
  console.log("Action: SpinReels - Spun Reels " + rand0 + ", " + rand1 + ", and " + rand2 + " times.");
  reels.reel0Reducer(reel0);
  reels.reel1Reducer(reel1);
  reels.reel2Reducer(reel2);
};

const CheckWin = (param) => {
  //Note this is a Helper Function for the DispenseWinnings Action
  console.log("Action: CheckWin - Called with param " + param + ".");
  let pot = API.getState().player.pot;
  let winnings = 0;
  if (param[0] === param[1] && param[0] === param[2]) {
    if (param[0] < 11) {winnings = pot * 10;} 
    else if (param[0] === 11 || param[0] === 12) {winnings = pot * 20;} 
    else if (param[0] === 13) {winnings = pot * 40;} 
    else if (param[0] === 14) {winnings = pot * 100;} 
    else if (param[0] === 15) {winnings = pot * 200;} 
    else if (param[0] === 16) {winnings = pot * 1000;}
  } 
  else if ((param[0] < 11) & (param[1] < 11) & (param[2] < 11)) {winnings = pot * 2;} 
  else if ((param[0] === 11 || param[0] === 12) && (param[1] === 11 || param[1] === 12) && (param[2] === 12 || param[2] === 12)) {winnings = pot * 5;} 
  else if ((param[0] === 13 || param[0] === 14 || param[0] === 15) && (param[1] === 13 || param[1] === 14 || param[1] === 15) && (param[2] === 13 || param[2] === 14 || param[2] === 15)) {winnings = pot * 20;}

  return winnings;
};

const CheckDoubles = (param) => {
  //Note this is a Helper Function for the DispenseWinnings Action
  console.log("Action: CheckDoubles - Called with param " + param + ".");
  let doubles = 0;
  let i = 0;
  for (i = 0; i < 3; i++){
    if (param[i] === 16) {
      doubles += 1;
    }
  }

  return doubles;
}

const DispenseWinnings = () => {
  //Note this Action requires CheckWin and CheckDoubles Helper Functions (local) 
  let player = API.getState().player;
  let reels = API.getState().reels;
  let visibleReel = reels.reelVisibleReducer();
  console.log("Action: DispenseWinnings - Initialized.");
  let winnings = 0;
  let doubles = 1;
  let line1 = [visibleReel[1], visibleReel[4], visibleReel[7]];
  winnings += CheckWin(line1);
  doubles += CheckDoubles(line1);
  if (player.pot > 1) {
    let line2 = [visibleReel[0], visibleReel[3], visibleReel[6]];
    winnings += CheckWin(line2);
    doubles += CheckDoubles(line2);
    if (player.pot > 2) {
      let line3 = [visibleReel[2], visibleReel[5], visibleReel[8]];
      winnings += CheckWin(line3);
      doubles += CheckDoubles(line3);
      if (player.pot > 3) {
        let line4 = [visibleReel[2], visibleReel[4], visibleReel[6]];
        winnings += CheckWin(line4);
        if (player.pot > 4) {
          let line5 = [visibleReel[0], visibleReel[4], visibleReel[8]];
          winnings += CheckWin(line5);
        }
      }
    }
  }
  player.purseReducer(0.5 * winnings * Math.pow(2, doubles));
  player.potEmptyReducer();
};

export { PlaceBet, InitCabinet, SpinReels, DispenseWinnings };
