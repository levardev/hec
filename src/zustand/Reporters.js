import API from "./API";
import PrintSlot from "../utilities/PrintSlot";

const ReportPlayer = () => {
  let player = API.getState().player;
  console.log("Reporter: Player - The Player has $" + player.purse + ".");
};

const ReportRound = () => {
  let count = API.getState().round.count;
  console.log("Reporter: Round - Round " + count + " is over.");
};

const ReportSlots = () => {
  let reels = API.getState().reels;
  let visibleReel = reels.reelVisibleReducer();
  console.log("Reporter:    ____________________________________________");
  console.log("Reporter:    |||" + PrintSlot(visibleReel[0]) + "|" + PrintSlot(visibleReel[3]) + "|" + PrintSlot(visibleReel[6]) + "|||");
  console.log("Reporter:    |||" + PrintSlot(visibleReel[1]) + "|" + PrintSlot(visibleReel[4]) + "|" + PrintSlot(visibleReel[7]) + "|||");
  console.log("Reporter:    |||" + PrintSlot(visibleReel[2]) + "|" + PrintSlot(visibleReel[5]) + "|" + PrintSlot(visibleReel[8]) + "|||");
  console.log("Reporter:    ____________________________________________");
};

export { ReportPlayer, ReportRound, ReportSlots };
