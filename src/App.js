import React from "react";
import MainGame from "./components/MainGame";

const App = () => {
  return <MainGame />;
};

export default App;
