import { PlaceBet, SpinReels, DispenseWinnings } from "../zustand/Actions";
import { ReportPlayer, ReportRound, ReportSlots } from "../zustand/Reporters";
import RTD from "../utilities/RTD"

const PlayRound = (event) => {
  console.log("Component: PlayRound - Intialized.")
  var key = event.keyCode;
  switch(key){
    case 50:
      PlaceBet(2);
      break;
    case 51:
      PlaceBet(3);
      break;
    case 52:
      PlaceBet(4);
      break;
    case 53:
      PlaceBet(5);
      break;
    default:
      PlaceBet(1);
      break;
  }
  SpinReels(RTD(0,100), RTD(0,100), RTD(0,100));
  DispenseWinnings();
  ReportSlots();
  ReportPlayer();
  ReportRound();
}

export default PlayRound