import {InitCabinet} from "../zustand/Actions";
import {ReportPlayer} from "../zustand/Reporters";
import PlayRound from "./PlayRound"

const MainGame = () => {
  console.log("Component: MainGame - Initialized.")
  InitCabinet();
  ReportPlayer();
  document.addEventListener('keydown', PlayRound);
  return null;
};

export default MainGame;
