const PrintSlot = (param) => {
  switch (param) {
    case 0:
      return "_Watermelon_";
    case 1:
      return "____Lime____";
    case 2:
      return "___Cherry___";
    case 3:
      return "___Orange___";
    case 4:
      return "___Grape____";
    case 5:
      return "___Banana___";
    case 6:
      return "___Clover___";
    case 7:
      return "__Diamond___";
    case 8:
      return "____Bell____";
    case 9:
      return "_Horseshoe__";
    case 10:
      return "_Poker Chip_";
    case 11:
      return "_Half Evil__";
    case 12:
      return "_Faze Clan__";
    case 13:
      return "_____3______";
    case 14:
      return "____33______";
    case 15:
      return "____333_____";
    case 16:
      return "____2X$_____";
    default:
      break;
  }
};

export default PrintSlot;
  